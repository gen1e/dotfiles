# Gen1e's Dotfiles

Various customization files, mostly terminal dotfiles.

## ZSH

ZSH dotfiles are included. I use [oh my zsh](https://ohmyz.sh/#install) for plugins and oh my posh with the dracula theme. For oh my posh I use the Robot nerd font it looks the best with the dracula theme. I also use the [zsh auto-suggestions plugin which needs to be installed manually](https://github.com/zsh-users/zsh-autosuggestions/blob/master/INSTALL.md#oh-my-zsh) and the [zsh-syntax-highlighting](https://github.com/zsh-users/zsh-syntax-highlighting/blob/master/INSTALL.md#oh-my-zsh) which also has it's own dracula theme.

[Brew](https://brew.sh/) is also needed for some of my plugins

I use tilix for Linux and I use the dracula theme for it.

## Other Commandline Tools

[The fuck](https://github.com/nvbn/thefuck#installation)

## Vim

The vimrc has everything, the pulgins are managed by vim-plug, which is setup to auto update.
[Installing vim-plug](https://opensource.com/article/20/2/how-install-vim-plugins), and remember to install plugins on first load.


